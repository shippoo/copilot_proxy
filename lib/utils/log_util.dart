import 'dart:io';

final _logFile = File('copilot_proxy.log');

bool noLog = false;

void log(Object? object, [bool printToConsole = true]) {
  if (!noLog) _logFile.writeAsStringSync('$object\n', mode: FileMode.append);
  if (printToConsole) print(object);
}
