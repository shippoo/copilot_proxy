//This file contains the Config class and related methods.
import 'package:copilot_proxy/utils/json_util.dart';

class Config {
  final String listenIp;
  final int listenPort;
  final List<String> httpProxyAddrList;
  final List<String> githubTokenList;
  final String tokenSalt;
  final String adminToken;
  final int copilotDebounce;
  String? configFilePath;

  Config({
    required this.listenIp,
    required this.listenPort,
    required this.httpProxyAddrList,
    required this.githubTokenList,
    required this.tokenSalt,
    required this.adminToken,
    required this.copilotDebounce,
  });

  factory Config.fromJson(JsonMap configMap) {
    return Config(
      listenIp: configMap['listenIp'] ?? '0.0.0.0',
      listenPort: configMap['listenPort'] ?? 8080,
      httpProxyAddrList: configMap['httpProxyAddrList'].cast<String>() ?? [],
      githubTokenList: configMap['githubTokenList'].cast<String>() ?? [],
      tokenSalt: configMap['tokenSalt'] ?? 'default_salt',
      adminToken: configMap['adminToken'] ?? 'default_admin_token',
      copilotDebounce: configMap['copilotDebounce'] ?? 1000,
    );
  }
}
