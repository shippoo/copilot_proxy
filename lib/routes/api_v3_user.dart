//This file contains the handler for the /api/v3/user route.
import 'package:copilot_proxy/check_auth/access_token_check_auth.dart';
import 'package:copilot_proxy/context.dart';

Future<void> getApiV3User(Context context) async {
  final token = context['token'];
  final userInfo = getUserInfoByAccessToken(token);
  context.ok();
  context.json(userInfo);
}
